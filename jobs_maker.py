import shutil
import os
import json
import re
import glob
import pdb
from datetime import datetime
import pandas as pd

class JobsMaker:
    '''
    This is the class to fold the job template.
    '''

    scinot = re.compile('[^A-Aa-z]([+\-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?)')

    # default constructor
    def __init__(self, jobs_df):
        self.jobs_df = jobs_df
        self._date = datetime.now().strftime("%Y_%m_%d_%H_%M_%S_")
        self.generation_filename = self._date + 'generation.json'
        self.sibling_filename = self._date + 'sibling.json'

    def save_generation_df(self):
        '''
        Save the generation df in a file
        '''
        with open(self.generation_filename, 'w') as generation_file:
            json.dump(self.jobs_df.to_dict(), generation_file, indent=4)

    def save_sibling_df(self,row):
        '''
        Save the sibling df in a file 
        '''
        with open(os.path.join(row.sibling_folder, self.sibling_filename), 'w') as sibling_file:
            json.dump(row.to_dict(), sibling_file, indent=4)

    def get_child_from_parent(self, row):
        shutil.copytree(row.parent_folder, row.child_folder)
        for ii in row.mutating_files:
            self.mutation(row,row.child_folder+'/'+ii)
        self.save_child_description(row)

    def mutation(self, row, my_file):
        #my_files = glob.glob(row.child_folder+'/*')
        #pdb.set_trace()
        with open(my_file, 'r') as file:
            template = file.read()
        with open(my_file, 'w') as file:
            file.write(self.make_mutation(template, row.to_dict(), '#!!'))

    def render_obsolete(self, template, replacement_dict, magic):
        'From Riccardo De Maria'
        out=[]
        for line in template.splitlines():
            if magic in line:
                orig,repl = line.split(magic,1)
                ll = repl.split()
                idx = 0 if len(ll)==1 else int(ll[1])
                name = ll[0]
                num = self.scinot.findall(orig)[idx]
                line = line.replace(num,str(replacement_dict[name]))
            out.append(line)
        return '\n'.join(out)

    def make_mutation(self, template, replacement_dict, magic):
        out=[]
        for line in template.splitlines():
            if magic in line:
                aux = line.split(magic)
                orig = aux[0]
                repl = aux[1]
                name = repl.strip()
                line = line.replace(orig,str(replacement_dict[name])+' ')
            out.append(line)
        return '\n'.join(out)

    def replace_parameter(self, my_file, i):
        pass

    def make_folder(self):
        '''
        Make folders from the job_template
        '''
        for index, row in self.jobs_df.iterrows():
            print(row['child_folder'])
            try:
                self.get_child_from_parent(row)
                self.jobs_df.loc[index,'child_folder_created']=True
            except FileExistsError:
                print(f'The child folder {row.child_folder} exists.')
                row['child_folder_created']=False
            except FileNotFoundError:
                print(f'The parent folder {row.parent_folder} does not exist.')
        self.save_parent_description()
