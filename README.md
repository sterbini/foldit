# jobs_maker


## Introduction
A beam phycisist (BP) should be as close as possible to the physics of the beam.

Parametric scans of numerical models flourished thanks to the availability 
of unprecedented computing power and the lack, the limit or complexity of the analytical tools. 

The focus of the BP should be on the physics and not on the technicalities of the numerical simulations.
This package aims to contribute in relieve the BP from this unnecessary burden.

The "job" is the atomic unit of the simulation step and it contains all the relevant physics. 
As such, the BP should harness it.

A "job" ultimately is a code running on a standalone pc.

The BP can launch several "sibling" jobs almost identical to the "parent" job with the exception of one or more "mutations".
This set of sibling jobs can be referred as "generation".
E.g., from a given and initial "pymask" job with tune (.31,.32) we want to generate a tune scan grid of 10x10 jobs with differen tunes (a tune scan). 
This is a "generation".

Each jobs can be "parent" of other jobs thus propagating a second generation.

The jobs can be run in parallel, locally or remotely.

There are different phases:
- define the parent job.
- apply a cloning
- apply a mutation
- apply another generic operation (like the execution or the postprocessing).

The jobs dataframe is the "map" we use to define all these "apply" (here you can see we are usign pandas terminology).

This package is intended to create the corresponding folders of the "generations" of the BP's study.

We suggest to use `yaml` files for the configurations: the `None` object of `python` is translated as `null` in `yaml`.

You can use the lxplus python distribution. Login in lxplus and then:
```
source /afs/cern.ch/eng/tracking-tools/python_installations/activate_default_python
```
