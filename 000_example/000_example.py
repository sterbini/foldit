import shutil
import os
import json
import pandas as pd
import ruamel.yaml
from pandarallel import pandarallel
import numpy as np


# Make the generation dataframe
cwd=os.path.abspath(os.getcwd())
nn=20
a=np.random.randn(nn,)
b=np.random.randn(nn,)
jobs_dict = {
             'parent_folder':cwd+'/000_job',
             'sibling_folder':[cwd+f'/000_generation/{ii:03d}_sibling_job/' for ii in range(nn)],
             'a': a,
             'b': b,
            }

# sibling_folder and parent_folder are needed columns!
jobs_df = pd.DataFrame(jobs_dict)

# The sweet functions
def clone(row):
    try:
        shutil.copytree(row.parent_folder, row.sibling_folder)
        return True
    except:
        return False
        
def mutate(row):
    #https://stackoverflow.com/questions/7255885/save-dump-a-yaml-file-with-comments-in-pyyaml
    yaml = ruamel.yaml.YAML()
    with open(row.sibling_folder+'/config.yaml', 'r') as file:
        cfg = yaml.load(file)
    
    cfg['a']=row['a']
    cfg['b']=row['b']
    
    with open(row.sibling_folder+'/config.yaml', 'w') as file:
        yaml.dump(cfg, file)

def save_sibling_json(row):
    '''
    Save the sibling df in a file 
    '''
    try:
        yaml = ruamel.yaml.YAML()
        with open(os.path.join(row.sibling_folder, 'sibling.yaml'), 'w') as sibling_file:
            yaml.dump(row.to_dict(), sibling_file)
        return True
    except:
        return False
        
def save_generation_df(jobs_df,generation_filename):
    '''
    Save the generation df in a file. We are suggesting YAML file beacause in our tests the json format DOES NOT maintain the numerical precision.
    In addition we suggest YAML instead of parquet because we are preferring an ASCII format for this light data of configuration (useful for inspections).
    Unfortunately pd df does not support YAML natively but it is not a big problem.
    '''
    yaml = ruamel.yaml.YAML()
    with open(generation_filename, 'w') as generation_file:
        yaml.dump(jobs_df.to_dict(), generation_file)

def load_generation_df(generation_filename):
    '''
    Load the generation df into a pd df. We are suggesting YAML file beacause in our tests the json format DOES NOT maintain the numerical precision.
    In addition we suggest YAML instead of parquet because we are preferring an ASCII format for this light data of configuration (useful for inspections).
    Unfortunately pd df does not support YAML natively but it is not a big problem.
    '''
    yaml = ruamel.yaml.YAML()
    with open(generation_filename, 'r') as generation_file:
        return pd.DataFrame(yaml.load(generation_file))
    
def run_it(row, my_command):
    return os.system(f'cd {row.sibling_folder};{my_command};')

def postprocess_it(row):
    with open(os.path.join(row.sibling_folder, 'output.yaml'), 'r') as file:
        yaml = ruamel.yaml.YAML()
        return yaml.load(file)['result']

#pandarallel.initialize(nb_workers=4,
#    progress_bar=True,
#    verbose=2,)

# The code
os.system('./cleanall.sh')

jobs_df.apply(clone, axis=1)
jobs_df.apply(mutate, axis=1)
jobs_df.apply(save_sibling_json, axis=1)
save_generation_df(jobs_df, 'generation.yaml')
jobs_df.apply(lambda x: run_it(x,'python run.py'),axis=1)
#jobs_df.parallel_apply(lambda x: run_it(x,'condor_submit run.sub'),axis=1)

# Postprocess
jobs_df.apply(postprocess_it,axis=1) 
print(jobs_df.apply(postprocess_it,axis=1).sort_index()==(b+a))
print('The END') 


