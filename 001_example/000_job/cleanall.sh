#! /bin/bash
rm fc.*
rm bb_len*
rm *pkl
rm *log
find -type l -delete
rm -rf  __pycache__ beambeam_macros db5 lhc out output temp slhc twiss* wise *.tfs mask_code myoptics.madx fidel last* main.mask.unmasked mad_generated verified pysixtrack
